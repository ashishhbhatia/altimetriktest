package altimetrik.com.altimetriktest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.altimetrik.pojo.ContactHolder;
import com.altimetrik.pojo.MyContacts;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Adapter class for displaying the row of contacts
 */
public class ContactsAdapter extends BaseAdapter {

        private List<MyContacts> data;
        private List<MyContacts> wholedata;
        Context context;
        ContactHolder contactHolder;


        public ContactsAdapter(List<MyContacts> contacts, Context context) {
            data = contacts;
            this.context = context;
            this.wholedata = new ArrayList<MyContacts>();
            this.wholedata.addAll(data);
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int i) {
            return data.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup) {
            View view = convertView;
            if (view == null) {
                LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                view = li.inflate(R.layout.contacts_list_item, null);
            } else {
                view = convertView;
            }

            contactHolder = new ContactHolder();
            contactHolder.name = (TextView) view.findViewById(R.id.name);
            contactHolder.phone = (TextView) view.findViewById(R.id.contactNumber);

            final MyContacts contact = (MyContacts) data.get(i);
            contactHolder.name.setText(contact.getName());
            contactHolder.phone.setText(contact.getNumber());

            view.setTag(data);
            return view;
        }

    /**
     * filter function for update on value change
     * @param charText
     */
        public void filter(String charText) {
            charText = charText.toLowerCase(Locale.getDefault());
            data.clear();
            if (charText.length() == 0) {
                data.addAll(wholedata);
            } else {
                for (MyContacts myContacts : wholedata) {
                    if (myContacts.getName().toLowerCase()
                            .contains(charText) || myContacts.getNumber().toLowerCase()
                            .contains(charText) ) {
                        data.add(myContacts);
                    }
                }
            }
            notifyDataSetChanged();
        }

}
